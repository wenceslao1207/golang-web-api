# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Start from golang v1.11 base image
FROM golang:1.12

# Add Mainteiner Info
LABEL maintainer="Wenceslao Marquez Burgos"
WORKDIR $GOPATH/src/gitlab.com/wenceslao1207/g-isi/src/g-isi-server

COPY ./src/g-isi-server/  .

RUN go get -d  -v ./...

RUN go install -v ./...

EXPOSE 8081

CMD ["g-isi-server"]



