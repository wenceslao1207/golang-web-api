package dao

import (
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-server/models"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"strconv"
)

type SubjectsDAO struct {
	Server   string
	Database string
}

var years = []string{"", "1 (Primer Anio)", "2 (Segundo Anio)", "3 (Tercer Anio)", "4 (Cuarto Anio)", "5 (Quinto Anio)"}

var db *mgo.Database

const (
	COLLECTION = "subjects"
)

// Conection to the DB
func (s *SubjectsDAO) Connect() {
	session, err := mgo.Dial(s.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(s.Database)
}

// Find list of subjects
func (s *SubjectsDAO) FindAll() ([]Subject, error) {
	var subjects []Subject
	err := db.C(COLLECTION).Find(bson.M{}).Sort("year").All(&subjects)
	return subjects, err
}

// Find a subjects based on its ID
func (s *SubjectsDAO) FindById(id string) ([]Subject, error) {
	var subject []Subject
	key, err := strconv.Atoi(id)
	if err != nil {
		return nil, err
	}
	anio := years[key]
	err = db.C(COLLECTION).Find(bson.M{"year": anio}).Sort("year").All(&subject)
	return subject, err
}

// Fin a subjects based on its ID
func (s *SubjectsDAO) FindByName(name string) ([]Subject, error) {
	var subject []Subject
	regex := bson.M{"$regex": bson.RegEx{Pattern: name}}
	err := db.C(COLLECTION).Find(bson.M{"name": regex}).Sort("name").All(&subject)
	return subject, err
}

func (s *SubjectsDAO) FindByNameSpecific(name string) (Subject, error) {
	var subject = Subject{}
	err := db.C(COLLECTION).Find(bson.M{"name": name}).One(&subject)
	return subject, err
}

func (s *SubjectsDAO) Insert(subject Subject) error {
	_, err := s.FindByNameSpecific(subject.Name)
	if err != nil {
		subject.ID = bson.NewObjectId()
		err = db.C(COLLECTION).Insert(&subject)
		return err
	}
	return err
}

func (s *SubjectsDAO) Delete(subject Subject) error {
	err := db.C(COLLECTION).Remove(&subject)
	return err
}

func (s *SubjectsDAO) Update(subject Subject) error {
	err := db.C(COLLECTION).Update(bson.M{"_id": subject.ID}, subject)
	return err
}
