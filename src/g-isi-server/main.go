package main

import (
	"fmt"
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-server/config"
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-server/dao"
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-server/handlers"
	"os"
	"os/exec"
)

var passPhrase = os.Getenv("SERVER_SIDE_KEY")

var config = Config{}

var dao = SubjectsDAO{}

var mySigingKey = []byte(passPhrase)

func clearScreen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func init() {
	config.Read()
	dao.Server = config.Server
	dao.Database = config.Database
	dao.Connect()
}

func main() {
	clearScreen()
	fmt.Println("Server running on port 8081")
	HandleRequests()
}
