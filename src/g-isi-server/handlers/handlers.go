package handlers

import (
	"encoding/json"
	"errors"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-server/dao"
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-server/models"
	"log"
	"net/http"
)

var passPhrase = []byte("Lordlao-1207|Wenceslao")

var dao = SubjectsDAO{}

var mySigingKey = []byte(passPhrase)

func stringInSlice(key string) bool {
	list := [5]string{"1", "2", "3", "4", "5"}
	for _, element := range list {
		if element == key {
			return true
		}
	}
	return false
}

func returnJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

func returnErrorJson(w http.ResponseWriter, code int, msg string) {
	returnJson(w, code, map[string]string{"error": msg})
}

func getSubjects(w http.ResponseWriter, r *http.Request) {
	subjects, err := dao.FindAll()
	if err != nil {
		returnErrorJson(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	returnJson(w, http.StatusOK, subjects)
}

func get(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	if stringInSlice(params["id"]) {
		getSubject(w, r, params["id"])
	} else {
		getSubjectByName(w, r, params["id"])
	}
}

func getSubject(w http.ResponseWriter, r *http.Request, params string) {
	subject, err := dao.FindById(params)
	if err != nil {
		returnErrorJson(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	returnJson(w, http.StatusOK, subject)
}

func getSubjectByName(w http.ResponseWriter, r *http.Request, params string) {
	subject, err := dao.FindByName(params)
	if err != nil {
		returnErrorJson(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	returnJson(w, http.StatusOK, subject)
}

func addSubject(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var subject Subject
	if err := json.NewDecoder(r.Body).Decode(&subject); err != nil {
		returnErrorJson(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := dao.Insert(subject); err != nil {
		returnErrorJson(w, http.StatusInternalServerError, err.Error())
		return
	}
	returnJson(w, http.StatusOK, subject)
}

func updateSubject(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var subject Subject
	if err := json.NewDecoder(r.Body).Decode(&subject); err != nil {
		returnErrorJson(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := dao.Update(subject); err != nil {
		returnErrorJson(w, http.StatusInternalServerError, err.Error())
		return
	}
	returnJson(w, http.StatusOK, map[string]string{"result": "Success"})
}

func delSubject(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var subject Subject
	if err := json.NewDecoder(r.Body).Decode(&subject); err != nil {
		returnErrorJson(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	if err := dao.Delete(subject); err != nil {
		returnErrorJson(w, http.StatusInternalServerError, err.Error())
		return
	}
	returnJson(w, http.StatusOK, map[string]string{"result": "Success"})
}

func getSubjectFullName(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	params := mux.Vars(r)
	subject, err := dao.FindByNameSpecific(params["name"])
	if err != nil {
		returnErrorJson(w, http.StatusBadRequest, "Invalid request payload")
		return
	}
	returnJson(w, http.StatusOK, subject)
}

func isAuthorized(endpoint func(http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header["Token"] != nil {
			token, err := jwt.Parse(r.Header["Token"][0], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, errors.New("There was an error")
				}
				return mySigingKey, nil
			})

			if err != nil {
				panic(err)
			}

			if token.Valid {
				endpoint(w, r)
			}

		} else {
			returnErrorJson(w, http.StatusInternalServerError, "Not Authorized")
			return
		}

	})
}

func HandleRequests() {
	router := mux.NewRouter()
	router.Handle("/subjects", isAuthorized(getSubjects)).Methods("GET")
	router.Handle("/subjects/{id}", isAuthorized(get)).Methods("GET")
	router.Handle("/subjects/", isAuthorized(addSubject)).Methods("POST")
	router.Handle("/subjects/{id}", isAuthorized(updateSubject)).Methods("PUT")
	router.Handle("/subjects/{id}", isAuthorized(delSubject)).Methods("DELETE")
	router.Handle("/subjects/name/{name}", isAuthorized(getSubjectFullName)).Methods("GET")

	if err := http.ListenAndServe(":8081", router); err != nil {
		log.Fatal(err)
	}
}
