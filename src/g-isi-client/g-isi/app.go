package main

import (
	"errors"
	"fmt"
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-client/functionhandler"
	"log"
	"os"
	"strconv"
)

var handler = NewFunctionHandler()

func main() {
	arguments := os.Args
	if len(arguments) > 1 {
		switch arguments[1] {
		case "-a":
			if len(arguments) > 2 {
				json, err := handler.ReadJSONFile(arguments[2])
				if err != nil {
					Error(err)
				}
				handler.AddNewEntry(json)
				return
			}
			err := handler.AddNewEntry()
			Error(err)
			subjects, err := handler.GetSubjects()
			Error(err)
			handler.PrintSubject(subjects)
		case "-h":
			handler.HandleErrorHelp("")
		case "-u":
			out, err := handler.UpdateEntry(arguments[2])
			Error(err)
			fmt.Println(out)
		case "1", "2", "3", "4", "5":
			arg, err := strconv.Atoi(arguments[1])
			Error(err)
			handler.GetSubjects(arg)
		case "-n", "--name":
			if len(arguments) > 2 {
				subject, err := handler.GetSubjectByName(arguments[2])
				Error(err)
				handler.PrintSubject(subject)
			} else {
				handler.HandleErrorHelp("Error: Opcion no valida")
			}
		case "-d", "--delete":
			var err error
			if len(arguments) > 2 {
				_, err = handler.DeleteEntry(arguments[2])
			} else {
				err = errors.New("Error: Opcion no valida")
			}
			Error(err)
		default:
			handler.HandleErrorHelp("Error: opcion no valida " + "\"" + arguments[1] + "\"")
		}
	} else {
		list, err := handler.GetSubjects()
		Error(err)
		handler.PrintSubject(list)
	}
	os.Exit(0)
}

func Error(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
