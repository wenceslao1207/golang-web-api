package functionhandler

import (
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-client/subjects"
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-client/token"
)

var subject = Subject{}
var subjects []Subject
var succes struct {
	Result string `json:"result"`
}

type FunctionHandler struct {
	years []string
	token string
}

func NewFunctionHandler() *FunctionHandler {
	token := Token{}
	tokenString, err := token.GenerateToken()
	if err != nil {
		panic(err)
	}
	f := new(FunctionHandler)
	f.token = tokenString
	f.years = []string{"", "1 (Primer Anio)", "2 (Segundo Anio)", "3 (Tercer Anio)", "4 (Cuarto Anio)", "5 (Quinto Anio)"}
	return f
}
