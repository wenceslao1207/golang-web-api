package functionhandler

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	. "gitlab.com/wenceslao1207/g-isi/src/g-isi-client/subjects"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
)

/*
 * Crud Function
 */
type mytype []map[string]string

func (f *FunctionHandler) GetSubjects(params ...int) ([]Subject, error) {
	var subject_json []byte
	var err error

	if len(params) >= 1 {
		subject_json, err = subject.GetAllSubjects(f.token, params[0])
	} else {
		subject_json, err = subject.GetAllSubjects(f.token)
	}

	if err != nil {
		return nil, err
	}

	json.Unmarshal(subject_json, &subjects)
	return subjects, nil
}

func (f *FunctionHandler) GetSubjectByName(param string) ([]Subject, error) {
	var subject_json []byte
	subject_json, err := subject.GetSubjectByName(f.token, param)

	if err != nil {
		return nil, err
	}

	json.Unmarshal(subject_json, &subjects)
	return subjects, nil
}

func (f *FunctionHandler) UpdateEntry(name string) ([]byte, error) {
	subjects, err := f.GetSubjectByName(name)
	if err != nil {
		return nil, err
	}
	subjectToUpdate := f.selectAnOption(subjects)
	subjectToUpdate, err = f.insertNewData(subjectToUpdate)
	if err != nil {
		return nil, err
	}
	jsonfile, err := json.MarshalIndent(subjectToUpdate, "", "")
	if err != nil {
		return nil, err
	}
	out, err := subject.UpdateSubject(f.token, jsonfile, subjectToUpdate.ID)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (f *FunctionHandler) DeleteEntry(name string) (string, error) {
	subjects, _ := f.GetSubjectByName(name)
	subjectToDelete := f.selectAnOption(subjects)
	response, err := subject.DeleteSubject(f.token, subjectToDelete)
	if err != nil {
		return "", errors.New("Opcion No valida")
	}
	json.Unmarshal(response, &succes)
	return succes.Result, nil
}

func (f *FunctionHandler) AddNewEntry(jsonfiles ...mytype) error {
	if len(jsonfiles) > 0 {
		for _, element := range jsonfiles {
			for _, subjectToAdd := range element {
				jsonString, err := json.Marshal(subjectToAdd)
				if err != nil {
					return err
				}
				_, err = subject.AddNewSubject(f.token, jsonString)
				if err != nil {
					return err
				}
			}
		}
		return nil

	}
	jsonfile, err := f.inputNew()
	if err != nil {
		return err
	}
	_, err = subject.AddNewSubject(f.token, jsonfile)
	if err != nil {
		return err
	}
	return nil
}

func (f *FunctionHandler) inputNew() ([]byte, error) {
	clearScreen()
	var jsonfile []byte
	fmt.Printf("\033[1m%s:\033[0m \n", "Go-UTN -- ADD NEW SUBJECT")
	name, err := f.inputReader("Subject Name")
	if err != nil {
		return jsonfile, err
	}
	year, err := f.inputReader("Year *number* (letters) ")
	if err != nil {
		return jsonfile, err
	}
	if !f.checkYear(year) {
		fmt.Println("Error Year not valid")
		os.Exit(1)
	}
	state, err := f.inputReader("Subject State")
	if err != nil {
		return jsonfile, err
	}
	subject = Subject{"", name, year, state}
	jsonfile, err = json.MarshalIndent(subject, "", "")
	if err != nil {
		return jsonfile, err
	}
	return jsonfile, nil
}

func (f *FunctionHandler) ReadJSONFile(filename string) (mytype, error) {
	var data []byte
	var array mytype
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return array, err
	}
	err = json.Unmarshal(data, &array)
	if err != nil {
		return array, err
	}
	return array, nil
}

func (f *FunctionHandler) selectAnOption(subjects []Subject) Subject {
	if len(subjects) > 1 {
		fmt.Printf("To select one inser index value starting in 0")
		indexString, _ := f.inputReader("New Subject Name: ")
		index, err := strconv.Atoi(indexString)
		if err != nil {
			panic(err)
		}
		return subjects[index]
	}
	return subjects[0]
}

func (f *FunctionHandler) insertNewData(subject Subject) (Subject, error) {
	clearScreen()
	fmt.Println("Update Subject")
	f.printLine(subject)
	name, err := f.inputReader("New Subject Name: (Leave empty to keep it)")
	if err != nil {
		return subject, err
	}
	if name != "" {
		subject.Name = name
	}
	fmt.Printf("\033[1m%s:\033[0m "+"%s \n", "Subject Name", subject.Name)
	year, err := f.inputReader("New Year: (Leave empty to keep it)")
	if err != nil {
		return subject, err
	}
	if year != "" {
		subject.Year = name
	}
	fmt.Printf("\033[1m%s:\033[0m "+"%s \n", "Year", subject.Year)
	state, err := f.inputReader("New State: (Leave empty to keep it)")
	if err != nil {
		return subject, err
	}
	if state != "" {
		subject.State = state
	}
	fmt.Printf("\033[1m%s:\033[0m "+"%s \n\n", "State", subject.State)
	return subject, nil
}

func clearScreen() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func (f *FunctionHandler) printSubject(subjects []Subject) {
	clearScreen()
	fmt.Printf("\033[1m%s\033[0m", "Go-UTN \n")
	if len(subjects) == 0 {
		fmt.Println("Nothing was found on the database")
	}
	for i := 0; i < len(subjects); i++ {
		f.printLine(subjects[i])
	}
}

func (f *FunctionHandler) PrintSubject(subjects []Subject) {
	clearScreen()
	fmt.Printf("\033[1m%s\033[0m", "Go-UTN \n")
	if len(subjects) == 0 {
		fmt.Println("Nothing was found on the database")
	}
	for i := 0; i < len(subjects); i++ {
		f.printLine(subjects[i])
	}
}

func (f *FunctionHandler) printLine(subject Subject) {
	fmt.Printf("\033[1m%s:\033[0m "+"%s \n", "Subject Name", subject.Name)
	fmt.Printf("\033[1m%s:\033[0m "+"%s \n", "Year", subject.Year)
	fmt.Printf("\033[1m%s:\033[0m "+"%s \n\n", "State", subject.State)
}

func (f *FunctionHandler) checkYear(year string) bool {
	for _, element := range f.years {
		if element == year {
			return true
		}
	}
	return false
}

func (f *FunctionHandler) inputReader(param string) (string, error) {
	fmt.Println("Insert " + param)
	reader := bufio.NewReader(os.Stdin)
	text, err := reader.ReadString('\n')
	if err != nil {
		return "", err
	}
	text = text[:len(text)-1]
	return text, nil
}

/*
 * Help function
 */

func (f *FunctionHandler) help() {
	fmt.Println("Options:")
	fmt.Println("	-a: {json file}	 Add new subject to database")
	fmt.Println("	-u: {json file}	 Update a subject")
	fmt.Println("	-d --delete: Delete subject from database")
	fmt.Println("	-n --name: {name} Search subjects based of string")
	fmt.Println("	-h: Show help message")
}

func (f *FunctionHandler) HandleErrorHelp(err string) {
	defer f.help()
	if err != "" {
		println(err)
	}
}
