package subjects

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

func (s *Subject) AddNewSubject(validToken string, jsonfile []byte) ([]byte, error) {
	client := http.Client{}
	requestLink := "http://localhost:8081/subjects/"
	req, err := http.NewRequest("POST", requestLink, bytes.NewBuffer(jsonfile))
	if err != nil {
		panic(err)
	}
	req.Header.Set("Token", validToken)
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return responseData, nil

}

func (s *Subject) GetAllSubjects(validToken string, params ...int) ([]byte, error) {
	var errorstring = []byte("")

	var requestLink string
	client := http.Client{}
	if len(params) == 1 {
		requestLink = "http://localhost:8081/subjects/" + strconv.Itoa(params[0])
	} else if len(params) > 1 {
		fmt.Println("ERROR")
	} else {
		requestLink = "http://localhost:8081/subjects"
	}

	req, err := http.NewRequest("GET", requestLink, nil)
	if err != nil {
		return errorstring, err
	}

	req.Header.Set("Token", validToken)
	response, err := client.Do(req)

	if err != nil {
		return errorstring, err
		os.Exit(1)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return errorstring, err
	}
	return responseData, nil

}

func (s *Subject) GetSubjectByName(validToken string, params string) ([]byte, error) {
	var errorstring = []byte("")

	var requestLink = "http://localhost:8081/subjects/" + params
	client := http.Client{}

	req, err := http.NewRequest("GET", requestLink, nil)
	if err != nil {
		return errorstring, err
	}

	req.Header.Set("Token", validToken)
	response, err := client.Do(req)

	if err != nil {
		return errorstring, err
		os.Exit(1)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return errorstring, err
	}

	return responseData, nil

}

func (s *Subject) DeleteSubject(validToken string, subject Subject) ([]byte, error) {
	var requestLink = "http://localhost:8081/subjects/"

	client := http.Client{}
	requestLink += subject.ID.String()

	jsonfile, err := json.MarshalIndent(subject, "", "")
	if err != nil {
		panic(err)
	}

	req, err := http.NewRequest("DELETE", requestLink, bytes.NewBuffer(jsonfile))
	if err != nil {
		panic(err)
	}

	req.Header.Set("Token", validToken)

	response, err := client.Do(req)
	if err != nil {
		return []byte(""), err
	}
	responseData, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return []byte(""), err
	}

	return responseData, nil
}

func (s *Subject) UpdateSubject(validToken string, subject []byte, id bson.ObjectId) ([]byte, error) {
	var requestLink = "http://localhost:8081/subjects/" + id.String()
	client := http.Client{}
	req, err := http.NewRequest("PUT", requestLink, bytes.NewBuffer(subject))
	if err != nil {
		panic(err)
	}

	req.Header.Set("Token", validToken)

	response, err := client.Do(req)
	if err != nil {
		return []byte(""), err
	}
	responseData, err := ioutil.ReadAll(response.Body)

	if err != nil {
		return []byte(""), err
	}
	return responseData, nil

}
