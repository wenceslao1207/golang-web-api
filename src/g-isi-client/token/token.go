package token

import (
	jwt "github.com/dgrijalva/jwt-go"
	"os"
	"time"
)

var clientside = os.Getenv("CLIENT_SIDE_KEY")

var mySigningKey = []byte(clientside)

type Token struct {
	key string
}

func (t *Token) GenerateToken() (string, error) {

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["client"] = "wmb1207"
	claims["exp"] = time.Now().Add(time.Minute * 30).Unix()

	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		return "", err
	}

	t.key = tokenString
	return t.key, nil
}
