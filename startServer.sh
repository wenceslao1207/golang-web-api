#! /usr/bin/env bash

DIRECTORY="$1"

if [[ ! -d "DIRECTORY" ]]; then
	mkdir $DIRECTORY
fi

docker run -d --net=host -v $DIRECTORY --name=GISIDB mongo

docker run -d --net=host --name=GISISERVER g-isi-server
